---
title: "Maximizando o Desempenho do MySQL: Estratégias Avançadas de Balanceamento de Carga"
categories: 
    - Banco de dados
date: "2024-02-13"
#lastmod: "2024-02-13"
#author: "Thiago"
cover: "/imgs/maximizando-o-desempenho-do-mysql-estrategias-avancadas-de-balanceamento-de-carga.jpg"
description: "Aqui apresentarei algumas dicas e estratégias práticas para otimizar o desempenho do seu banco de dados MySQL usando balanceamento de carga."
tags:
    - mysql
    - database
    - dba
    - loadbalancing
    - performance
    - proxysql
---

_Aqui apresentarei algumas dicas e estratégias práticas para otimizar o desempenho do seu banco de dados MySQL usando balanceamento de carga._

---

O MySQL é um aliado poderoso para armazenar e gerenciar dados, mas com o aumento da demanda, é natural que você queira extrair o máximo desempenho dele. Felizmente, existem técnicas simples e eficazes que podem ajudar. Neste artigo, vamos explorar dicas amigáveis e didáticas para otimizar o desempenho do MySQL por meio de balanceamento de carga e particionamento. __Vamos lá!__

## Configuração Inicial do MySQL
Antes de mergulharmos nas estratégias avançadas, vamos configurar nosso MySQL adequadamente. No arquivo de configuração `my.cnf`, ajuste parâmetros como `innodb_buffer_pool_size` e `max_connections` para otimizar o desempenho.

### Aumento de buffer
Por exemplo, para aumentar o tamanho do buffer do InnoDB, você pode adicionar a seguinte linha ao seu `my.cnf`:

```bash
innodb_buffer_pool_size = 2G
```
Isso aumentará o tamanho do buffer do InnoDB para 2 gigabytes. Certifique-se de ajustar de acordo com os recursos disponíveis em seu servidor.

### O mínimo de segurança
Além disso, para garantir a segurança, configure senhas fortes para suas contas de usuário e restrinja o acesso remoto apenas quando necessário. Por exemplo, para definir uma senha para o usuário __root__, você pode usar o seguinte comando SQL:

```sql
ALTER USER 'root'@'localhost' IDENTIFIED BY 'sua_senha_segura';
```

Substitua `'sua_senha_segura'` pela senha desejada. Lembre-se de fazer o mesmo para outros usuários, se aplicável.

Lembre-se de que se a senha ainda não foi criada você primeiro deve usar:

```sql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'nova_senha';
```

### Backup é sempre bom!
Por fim, estabeleça um plano de backup e recuperação usando o mysqldump. Por exemplo, para fazer um backup de um banco de dados chamado meu_banco, você pode usar o seguinte comando:

```bash
mysqldump -u seu_usuario -p seu_banco > backup.sql
```

Substitua seu_usuario pelo nome de usuário e seu_banco pelo nome do banco de dados. O arquivo backup.sql conterá o backup do seu banco de dados.

Essas são algumas etapas essenciais para configurar seu MySQL inicialmente. Certifique-se de revisar e ajustar conforme necessário para atender às necessidades específicas do seu ambiente.

## Balanceamento de Carga: Distribuindo a Carga de Forma Inteligente
O balanceamento de carga atua como um suporte essencial para o seu MySQL, distribuindo a carga de trabalho de forma inteligente entre os servidores. Essa técnica é crucial para garantir um desempenho consistente e uma utilização mais eficiente dos recursos disponíveis.

No balanceamento de carga de aplicativos com ProxySQL, é essencial configurar tanto os servidores MySQL (master e slave) quanto o ProxySQL para distribuir a carga de forma inteligente. 

### Instale e inicie o ProxySQL

Você pode baixar o ProxySQL do site oficial ou usar o gerenciador de pacotes da sua distribuição Linux. Por exemplo, para sistemas baseados em Debian/Ubuntu, você pode usar o apt:

```bash
wget -O - 'https://repo.proxysql.com/ProxySQL/proxysql-2.5.x/repo_pub_key' | sudo apt-key add -

echo deb https://repo.proxysql.com/ProxySQL/proxysql-2.5.x/$(lsb_release -sc)/ ./ | sudo tee /etc/apt/sources.list.d/proxysql.list

sudo apt-get update

sudo apt-get install -y --no-install-recommends lsb-release wget apt-transport-https ca-certificates gnupg

sudo apt-get install proxysql
```

Após a instalação, você pode iniciar o __ProxySQL__ usando o seguinte comando:

```bash
sudo systemctl start proxysql
```

Você também pode habilitar o __ProxySQL__ para iniciar automaticamente durante o boot do sistema:

```bash
sudo systemctl enable proxysql
```

### Configuração do Master e Slave MySQL

No servidor __MySQL__ que atuará como master, certifique-se de que a replicação esteja configurada corretamente. Você pode fazer isso definindo o servidor master no arquivo `my.cnf`:

```bash
server-id = 1
log-bin = /var/log/mysql/mysql-bin.log
binlog-do-db = database_name
```

Substitua `database_name` pelo nome do banco de dados que deseja replicar.

Também é necessário alterar o `server-id` do servidor MySQL que atuará como slave para garantir que ele tenha um ID de servidor único na configuração de replicação. Você pode fazer isso adicionando a seguinte linha ao arquivo `my.cnf` do servidor slave:

```bash
server-id = 2
```

Substitua 2 pelo ID de servidor desejado. Certifique-se de escolher um ID que seja diferente do ID do servidor master.

No servidor MySQL que atuará como slave, configure a replicação para se conectar ao master e aplicar as alterações do binlog:

```mysql
CHANGE MASTER TO 
   MASTER_HOST='master_ip', 
   MASTER_USER='replication_user', 
   MASTER_PASSWORD='replication_password', 
   MASTER_LOG_FILE='mysql-bin.000001', 
   MASTER_LOG_POS=123456;

START SLAVE;
```

Substitua `master_ip`, `replication_user` e `replication_password` pelos detalhes de conexão do servidor master.

Quanto ao `MASTER_LOG_FILE` e `MASTER_LOG_POS`, estes são parâmetros usados para indicar ao servidor slave onde começar a replicação no servidor master.

`MASTER_LOG_FILE`: Este parâmetro especifica o nome do arquivo binário de log no servidor master a partir do qual o servidor slave deve começar a replicar as alterações. Este arquivo contém um registro de todas as alterações no banco de dados. Por exemplo, __mysql-bin.000001__.

`MASTER_LOG_POS`: Este parâmetro especifica a posição no arquivo binário de log do servidor master a partir do qual o servidor slave deve começar a replicar as alterações. É uma posição dentro do arquivo binário de log. Por exemplo, __123456__.

Ao iniciar a replicação, o servidor slave inicia a leitura do arquivo binário de log especificado pelo MASTER_LOG_FILE a partir da posição indicada pelo MASTER_LOG_POS. Ele então aplica as alterações registradas nesse arquivo de log para manter os dados sincronizados com o servidor master.

Para obter o valor do MASTER_LOG_FILE e MASTER_LOG_POS, você pode executar o comando SHOW MASTER STATUS; no servidor master. Isso retornará o nome do arquivo binário de log atual (File) e a posição atual (Position). Esses valores podem então ser usados na configuração do servidor slave.

### Configuração do ProxySQL

No __ProxySQL__, adicione os servidores MySQL ao pool de servidores:   

```bash
mysql -u admin -padmin -h 127.0.0.1 -P 6032 -e "INSERT INTO mysql_servers(hostgroup_id,hostname,port) VALUES (10,'master_ip',3306),(20,'slave_ip',3306);"
```

Substitua `master_ip` e `slave_ip` pelos endereços IP dos servidores master e slave, respectivamente.

Defina regras de roteamento para distribuir as consultas entre os servidores:

```bash
mysql -u admin -padmin -h 127.0.0.1 -P 6032 -e "INSERT INTO mysql_query_rules (rule_id, active, match_digest, destination_hostgroup) VALUES (1,1,'^SELECT.*',20), (2,1,'^INSERT.*',10), (3,1,'^UPDATE.*',10), (4,1,'^DELETE.*',10);"
```

Neste exemplo, as consultas `SELECT` são roteadas para o grupo de hosts do slave (20).

Por fim, carregue as configurações para o __ProxySQL__:

```bash
mysql -u admin -padmin -h 127.0.0.1 -P 6032 -e "LOAD MYSQL SERVERS TO RUNTIME; SAVE MYSQL SERVERS TO DISK;"

mysql -u admin -padmin -h 127.0.0.1 -P 6032 -e "LOAD MYSQL QUERY RULES TO RUNTIME; SAVE MYSQL QUERY RULES TO DISK;"
```

__Explicando:__

1. O comando `LOAD MYSQL SERVERS TO RUNTIME;` carrega as configurações dos servidores MySQL para a memória em tempo de execução do ProxySQL, ou seja, as configurações são aplicadas imediatamente.

2. O comando `SAVE MYSQL SERVERS TO DISK;` salva as configurações dos servidores MySQL no disco, para que elas persistam após um reinício do ProxySQL.

Esses comandos são usados após adicionar, modificar ou remover servidores MySQL do ProxySQL. Eles garantem que as alterações sejam aplicadas e persistam.

1. O comando `LOAD MYSQL QUERY RULES TO RUNTIME;` carrega as regras de roteamento de consultas para a memória em tempo de execução do ProxySQL, aplicando as alterações imediatamente.

2. O comando `SAVE MYSQL QUERY RULES TO DISK;` salva as regras de roteamento de consultas no disco, garantindo que elas sejam preservadas após um reinício do ProxySQL.

Esses comandos são usados após adicionar, modificar ou remover regras de roteamento de consultas no ProxySQL. Eles garantem que as alterações nas regras sejam aplicadas e persistam.

### Configuração do ProxySQL através do arquivo `proxysql.cnf`:

O ProxySQL oferece a opção de configurar suas definições através de um arquivo de configuração chamado proxysql.cnf, facilitando a gestão das configurações. Vamos explorar como configurar o ProxySQL com algumas configurações básicas, incluindo credenciais de administração, servidores MySQL e regras de roteamento de consultas.

Aqui está um exemplo de como seria o arquivo proxysql.cnf com as configurações que discutimos:

```bash
# Arquivo de configuração do ProxySQL

# Configurações globais
datadir=/var/lib/proxysql

# Configurações de administração
admin_variables =
{
    admin_credentials="admin:admin_password"
}

# Configurações dos servidores MySQL
mysql_servers =
(
    {
        hostgroup_id=10,
        hostname='mysql1.example.com',
        port=3306,
        weight=100,
        max_connections=1000
    },
    {
        hostgroup_id=20,
        hostname='mysql2.example.com',
        port=3306,
        weight=100,
        max_connections=1000
    }
)

# Configurações de regras de roteamento de consultas
mysql_query_rules:
(
    {
        rule_id=1,
        active=1,
        match_digest='^SELECT.*',
        destination_hostgroup=20
    },
    {
        rule_id=2,
        active=1,
        match_digest='^INSERT.*',
        destination_hostgroup=10
    },
    {
        rule_id=3,
        active=1,
        match_digest='^UPDATE.*',
        destination_hostgroup=10
    },
    {
        rule_id=4,
        active=1,
        match_digest='^DELETE.*',
        destination_hostgroup=10
    }
)
```

### Alterando a senha de admin do ProxySQL

Conecte-se à interface de administração do ProxySQL usando um cliente MySQL:

```bash
mysql -u admin -padmin -h 127.0.0.1 -P 6032
```

A porta `6032` é a porta padrão usada para se conectar à interface de administração do __ProxySQL__.

Uma vez conectado à interface de administração, você pode alterar a senha do usuário admin usando o seguinte comando:

```bash
UPDATE global_variables SET variable_value='admin:nova_senha' WHERE variable_name='admin-admin_credentials';

LOAD ADMIN VARIABLES TO RUNTIME;
```

Saia do prompt do ProxySQL e tente conectar novamente. Mas lembre-se de agora usar as novas credenciais.

```bash
mysql -u admin -pnova_senha -h 127.0.0.1 -P 6032
```

Com essa configuração, o ProxySQL distribuirá automaticamente as consultas entre os servidores MySQL de acordo com as regras de roteamento definidas, permitindo um balanceamento de carga eficiente e inteligente em seu ambiente MySQL. Certifique-se de monitorar regularmente o desempenho e ajustar as configurações conforme necessário.

## Conclusão

Neste artigo, exploramos as técnicas essenciais de __balanceamento de carga__ para otimizar o desempenho de bancos de dados MySQL. Ao distribuir inteligentemente a carga de trabalho entre vários servidores MySQL podemos alcançar uma melhor utilização dos recursos disponíveis e uma resposta mais rápida às consultas dos usuários.

Ao implementar o balanceamento de carga com o __ProxySQL__, aprendemos como direcionar consultas de forma eficiente para os servidores MySQL disponíveis, garantindo que cada servidor seja utilizado de maneira equilibrada e que consultas de leitura e gravação sejam processadas de acordo com a configuração desejada.

Essa técnica não apenas ajudam a melhorar o desempenho do MySQL, mas também contribuem para a resiliência e a disponibilidade do sistema, distribuindo a carga de trabalho de forma mais uniforme e evitando gargalos em servidores individuais.

Em resumo, ao implementar estratégias eficazes de balanceamento de carga, podemos aproveitar ao máximo o potencial do MySQL, garantindo um desempenho robusto e confiável para nossas aplicações e serviços.

Se você busca melhorar a eficiência e o desempenho de seus bancos de dados MySQL, considere a implementação dessa técnica em sua arquitetura de banco de dados. Com a abordagem certa e as ferramentas adequadas, você pode alcançar um ambiente de banco de dados mais __ágil__, __escalável__ e __resiliente__ para atender às demandas de suas aplicações e usuários.

_Obrigado por ler nosso artigo e espero que as informações aqui apresentadas tenham sido úteis para sua jornada de otimização de bancos de dados MySQL. Se tiver alguma dúvida ou comentário, não hesite em entrar em contato. Estamos aqui para ajudar e compartilhar conhecimento._